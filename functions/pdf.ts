import middy from "middy";
import chromium from "chrome-aws-lambda";
import puppeteer, { Page } from "puppeteer-core";
import {
  doNotWaitForEmptyEventLoop,
  httpErrorHandler,
} from "middy/middlewares";

import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";

const LOCAL_CHROMIUM_PATH =
  "./node_modules/puppeteer/.local-chromium/mac-756035/chrome-mac/Chromium.app/Contents/MacOS/Chromium";
const KEY = process.env.ACCESS_KEY;
const FONT_PATH = process.env.FONT_PATH;

const init = async (): Promise<Page> => {
  const readFontProcess = chromium.font(FONT_PATH);

  const executablePath = process.env.IS_OFFLINE
  ? LOCAL_CHROMIUM_PATH
  : await chromium.executablePath;


  await readFontProcess;
  const browser = await puppeteer.launch({
    args: chromium.args,
    executablePath,
  });

  return browser
    .newPage()
    .then((page) =>
    {
      page.emulateMedia("screen")
      return page
    });
}

const handler = async (
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  // lambda上ではヘッダーのkeyが小文字になってしまう
  const accessKey = event.headers["x-lambda-access-key"];

  if (accessKey !== KEY) {
    return {
      statusCode: 400,
      body: "access key is incorrect.",
    };
  }

  let body = event.body;
  if (!body) {
    return {
      statusCode: 400,
      body: "body can't loaded.",
    };
  }

  if (event.isBase64Encoded) {
    const buff = Buffer.from(body, "base64");
    body = buff.toString("ascii");
  }

  const bodyObject = JSON.parse(body);

  const accessUrl = bodyObject.access_url;
  if (typeof accessUrl !== "string") {
    return {
      statusCode: 400,
      body: "access url can't loaded.",
    };
  }

  const accessToken = bodyObject.access_token;
  if (typeof accessToken !== "string") {
    return {
      statusCode: 400,
      body: "access token can't loaded.",
    };
  }

  const page = await init();

  await page.setRequestInterception(true).then(() => {
    page.on("request", (request) => {
      const headers = request.headers();
      headers["X-HOGEHOGE-TOKEN"] = accessToken;
      request.continue({
        headers,
      });
    });
  })

  // await page.goto(url, {
  //   waitUntil: ["networkidle0", "load", "domcontentloaded"]
  // });
  // TODO waitUntilの設定要確認
  await page.goto(accessUrl, {});

  const pdfStream = await page.pdf({
    printBackground: true,
    landscape: true,
  });

  const pdfData = pdfStream.toString("base64");

  return {
    statusCode: 200,
    isBase64Encoded: true,
    headers: {
      "Content-type": "application/pdf",
    },
    body: pdfData,
  };
};

export const generate = middy(handler)
  .use(doNotWaitForEmptyEventLoop())
  .use(httpErrorHandler());
