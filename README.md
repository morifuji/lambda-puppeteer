# lambda-puppeteer

run local.

```
$ yarn

# edit environment variables.
$ vi serverless.yml

# run local.
$ yarn serverless
```

you can generate pdf.

```
$ curl --request POST \
  --url http://localhost:3003/dev/puppeteer/pdf \
  --header 'content-type: application/json' \
  --header 'x-lambda-access-key: hogehoge' \
  --data '{
	"access_url": "https://www.google.com/",
	"access_token": "your_original_access_key"
}'
--output demo.pdf

# open pdf
$ open demo.pdf
```

deploy it by serverless framework.

```
$ sls deploy
```
